# 合规SIG例会 2023-2-3 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
- KHDVK-450A开发板上主干合规评审  王忠进
- RK3588开发板上主干合规遗留问题闭环评审    葛楠
- 合规SIG 2023年重点工作规划启动开工     高亮


## 与会人Attendees
- 陈雅旬、余甜、丛林、刘德帅、石磊、陈一雄、关仁杰、赵鹏、葛楠、周乃鑫、赵秀成、王忠进、缪嘉男、高亮

## 纪要Notes
- 议题1、KHDVK-450A开发板上主干合规评审

会议结论

经评审，整体基本满足合规要求，以下问题闭环后，可通过合规评审。

1、尽量减少mklfs.c 文件片段引用问题，mklfs.c文件是否可作为三方合适的镜像制作工具引入需与PMC董金光确认。

2、gd32f4xx_systick.c以及printf.c文件中copyright 格式中OAT告警问题整改。

- 议题2、RK3588开发板上主干合规遗留问题闭环评审

会议结论

经评审，合规评审遗留问题已闭环完毕。同意通过合规评审。

多License整改结果，可作为后续项目参考https://gitee.com/openharmony-sig/device_soc_rockchip/blob/master/LICENSE

- 议题3、合规SIG 2023年重点工作规划启动开工

会议结论

23年合规SIG 2023年重点工作规划收集启动，请合规SIG各成员，在下次例会前（2月17日），进行提交和填写。

重点工作规划收集链接： https://shimo.im/sheets/B1Aw1d18GeFygLqm/gofnm

## Action items
- 合规SIG成员，补充重点工作规划
