﻿# 	sig-content

### 索引

1. [sig维护框架](./Infrastructure/docs/manual/sig数据维护框架.md)
2. [sig的总入口](https://gitee.com/openharmony/community/tree/master/sig)
3. [gitee 提PR操作指导](./Infrastructure/docs/manual/gitee提pr常规操作.md)
4. [代码上sig仓流程]()
5. [代码/文档规范](https://gitee.com/openharmony/docs/tree/master/zh-cn/contribute)
6. [开源合规检查工具](https://gitee.com/openharmony-sig/tools_oat)
7. [对于主线已有库的维护方法](./Infrastructure/docs/manual/patch管理/针对修改已有库需求的管理方案.md)
8. [更新社区sig组信息](./Infrastructure/docs/manual/更新社区sig信息.md)
9. [SIG仓库门禁规则](./Infrastructure/docs/manual/sig_rules.md)
### 活动公告

1. [930线上见面会]( ./release_management/docs/930)

