#  November 16 2021 at 16:00pm-16:30pm GMT+8

## Agenda

- OH BlockChain SIG例会[会议录制链接](这次忘记录屏了，抱歉) 

  |    时间     | 议题                                               | 发言人                                                       |
  | :---------: | -------------------------------------------------- | ------------------------------------------------------------ |
  | 16:00-16:10 | 二阶段代码开发进展同步                           | 趣链-汪晓可                                           |
  | 16:10-16:20 | demo验证场景同步                   | 趣链-汪晓可                                           |
  | 16:20-16:30 | 自由讨论                         | 软件所-李凯，趣链-尚璇，趣链-汪晓可                                                 |
  

## Attendees

- 李凯
- [Lee](https://gitee.com/www.lee.com)
- [platformdepartment](https://gitee.com/platformdepartment)
- [lingfengbao](https://gitee.com/lingfengbao)
- [xkwang1228](https://gitee.com/xkwang1228)
- [shangxuan_hz](https://gitee.com/shangxuan_hz)
- [jiafeicheng](https://gitee.com/15335180944)

## Notes

- 议题一、同步上链能力开发进展
  
  介绍了最近一个月的开发进展，目前已在3861板子和树莓派上把demo跑通。
  
-  议题二、物联网 + 区块链智慧农业应用场景分享

  介绍了物联网 + 区块链智慧农业应用场景。端侧负责采集温湿度数据，边侧负责进行数据验证和上链，云侧负责数据存证，并基于智能合约预置的预警规则进行预警。最终形成了云边端协同的物联网 + 区块链的架构


- 议题三、自由讨论
  1. 李凯老师提出了问题，在这个场景中，区块链的应用价值是什么。趣链尚璇做出了回答，主要是两方面。首先，能够实现链上链下协同的数据可信采集和存证。其次，能够基于智能合约实现预警出发，不受人工干预。
  2. 关于新增代码仓的问题，已在内部确认中，本周会发邮件。


## Action Items

- 发邮件给PMC申请新增代码仓


  