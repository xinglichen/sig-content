#  Augest 31 2021 at 16:00pm-16:40pm GMT+8

## Agenda

- OH Dev-Board SIG例会[会议录制链接](https://meeting.tencent.com/v2/cloud-record/share?id=cf127de7-3126-4160-8217-d42b2df5212e&from=3&is-single=true) 访问密码：2rCk66PA

  |    时间     | 议题                                               | 发言人                                                       |
  | :---------: | -------------------------------------------------- | ------------------------------------------------------------ |
  | 16:00-16:10 | 上链能力开发进展同步                           | 趣链-汪晓可                                           |
  | 16:10-16:30 | 区块链黑匣子应用场景分享                       | 趣链-李世敬                                           |
  | 16:30-16:40 | 自由讨论                         | 软件所-李凯，趣链-尚璇，中央财大-陈波                                                  |
  

## Attendees

- 李凯
- [haisong2](https://gitee.com/haisong2)
- [Lee](https://gitee.com/www.lee.com)
- [platformdepartment](https://gitee.com/platformdepartment)
- [lingfengbao](https://gitee.com/lingfengbao)
- [xkwang1228](https://gitee.com/xkwang1228)
- [jerryma0912](https://gitee.com/jerryma0912)
- [shangxuan_hz](https://gitee.com/shangxuan_hz)
- [zhao_kuo](https://gitee.com/zhao_kuo)

## Notes

- 议题一、同步上链能力开发进展
  
  介绍了最近两周的开发进展，目前已在3861板子上把demo跑通，下一步将把上链能力进行移植。
  
-  议题二、区块链黑匣子应用场景分享

  介绍了区块链黑匣子在安防和汽车领域可能的应用场景。黑匣子内置上链能力，实现信息的可信记录、防篡改、可追溯。


- 议题三：自由讨论
  李凯老师提出了可以和车机接口sig进行交流，了解基于OH的车机信息同步相关的技术。
  陈波教授提出新能源汽车的电池信息上链，可以和碳中和场景进行结合。


## Action Items

- 与车机接口sig进行交流和学习
- 按照OH编码规范，对上链能力的代码进行重构和完善


  