# September.13, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips编译工具和框架适配进展同步|Lain 李兵|
|10:15-10:25|子系统适配进展同步|袁祥仁 黄首西|
|10:25-10:35|系统三方组件适配|Lain 黄首西 刘佳科|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips编译工具和框架适配进展同步

**结论**
- 1.芯联芯正在适配中，当前init报错，计划在本周和君正沟通，在9月底完成适配并验证子系统功能。

#### 议题二、子系统适配进展同步

**结论**
- 1.君正计划将图形子系统在9月底在板子上验证完效果。
- 2.升级子系统目前确定可以满足第三方适配需求，计划在L1系统功能增强需求完成后开始适配。
- 3.日期，国际化子系统编译正常，计划12月底前完成测试。


#### 议题三、系统三方组件适配进展同步

**结论**
- boringssl仓库已经退休，暂停适配。


## Action items
