#  December 8, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                    | 发言人                                            |
  | :---------: | ------------------------------------ - | ------------------------------------------------- |
  | 16:00-16:15 | 议题一、Media的codec驱动能力开发进展     | crescenthe,vb6174,zhangguorong,keivn              |
  | 16:10-16:30 | 议题二、light模块HDI接口评审             | chenfeng,yuanbo,kevin,YUJIA,liuhonggang                       |
  | 16:10-16:30 | 议题三、HDF&HDI框架代码自动化工具共建讨论 | chenfeng,yuanbo,kevin,bayanxin,zhaojunxia         |
  | 16:10-16:30 | 议题四、USB模块HDI接口评审               | chenfeng,yuanbo,kevin,YUJIA,wuchengwen,yuchao,liuhonggang     |
  | 16:10-16:30 | 议题五、display模块HDI接口评审           | chenfeng,yuanbo,kevin,YUJIA,liuhonggang                       |
## Attendees

- @crescenthe(https://gitee.com/crescenthe)
- @vb6174(https://gitee.com/vb6174) 
- @zhangguorong(zhangguorong050@chinasoftinc.com)
- @YUJIA(https://gitee.com/JasonYuJia)
- @yuanbo(https://gitee.com/yuanbogit)
- @Kevin-Lau(https://gitee.com/Kevin-Lau)
- @yuchao
- @wuchenwen(https://gitee.com/wu-chengwen)
- @zhaojunxia(zhaojunxia@kaihongdigi.com)
- @bayanxin(bayanxing@kaihongdigi.com)
- @zhangliangliang(zhangliangliang@kaihongdigi.com)
- @liuhonggang(https://gitee.com/liuhonggang123)

## Notes

- **议题一、Media的codec驱动能力开发进展**

   议题进展：已完成codec idl接口定义，并根据HDI-GEN工具生成代码进行适配，计划本周完成开发，下周拉通OpenMax和媒体启动联调。 

- **议题二、light模块HDI接口评审**

   议题结论：light模块新增3个接口，接口和数据设计合理，兼容性符合要求，接口评审通过。 

   新增接口列表如下： 

   | 接口                       | 功能描述                                  |
   | -------------------------- | ----------------------------------------- |
   | GetLightInfo               | 获取系统支持的light设备信息                |
   | TurnOnLight               | 按照亮度或者闪烁模式，打开light设备         |
   | TurnOffLight              | 关闭light设备                             |

- **议题三、HDF&HDI框架代码自动化工具共建讨论**

议题结论：会议分享已开发DevEcoDeviceTool HDF
Driver工具，HC-GEN工具，HDI-GEN等工具能力，与深开鸿各位专家讨论当前现状和需求规划，针对以下五个需求共建点展开深入讨论：

   1、HDI服务辅助开发工具
      idl文件编写-手写、.h导入.
      idl服务端开发，编译、调试.
      测试用例生成、测试. 
   2、驱动模型专有驱动模板生成工具. 
   3、用户态驱动快速烧录（so替换）、在线调试. 
   4、HC-GEN工具，具有HCS配置可视化能力. 
   5、HDI-GEN工具，具有.h 转换为.idl能力。 
针对以上讨论问题，后续进行深入交流，然后梳理需求规格进行sig和PMC评审，创建issue跟踪闭环。

- **议题四、USB模块HDI接口评审**
   议题结论：USB模块新增2个接口，接口和数据设计合理，兼容性符合要求，接口评审通过。 

   新增接口列表如下： 

   | 接口                            | 功能描述                                  |
   | --------------------------------| ----------------------------------------- |
   | BindUsbdSubscriber              | 绑定usb用户订阅信息                        |
   | UnbindUsbdSubscriber            | 取消绑定usb用户订阅信息                     |
遗留问题：增加UnbindUsbdSubscriber的入参（用户订阅信息，区分取消那个用户订阅）。

- **议题五、display模块HDI接口评审**

   议题结论：display模块，变更1个接口入参和2个数据结构，变更合理，兼容性符合要求，接口评审通过。

   变更后接口如下： 
   int32_t (*GetDisplaySuppportedModes)(uint32_t devId, uin32_t *num, DisplayModeInfo
   *modes);

会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/IM6RDCP5EDEYAF7CABIGYCSCHUN7O3R7/

会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 

会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
