# May 7, 2022 at 09:30am GMT+8

## Agenda
- Python SIG 教育方向研讨会

## Attendees
- 黄庆典
- 詹敏昌
- 白俊鸽
- 薛小铃
- 龙芯俱乐部
- 微笑的Rockets
- 裴嘉@小熊派
- 徐派老师
- 唐佐林
- 梁嘉升
- 向金
- 凯
- 冯建江
- Descartes
- 孙楷丰

## Notes
- 链接：https://pan.baidu.com/s/15RJ2W8V-i-sDo5PuSwyGBQ 
  提取码：bgsd 
- 中小学信息教育新课标解读
- PZStudio 简介
- Python SIG 未来工作方向

## Action items
- PZStudio到Py4OH的适配
- Py4OH到小凌派开发板的适配
- 省级编程赛事的支持
