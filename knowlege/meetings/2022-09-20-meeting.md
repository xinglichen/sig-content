# Sep 20, 2022 at 15:00pm GMT+8

## Agenda
+ 遗留问题汇总
+ 知识体系联合立项进展
+ 知识体系知识梳理

## Attendees
- [zeeman_wang](https://gitee.com/zeeman_wang)
- [张路](zhanglu@iscas.ac.cn)
- [madixin](https://gitee.com/madixin)
- [Cruise2019](https://gitee.com/Cruise2019)
- [深开鸿-guoyuefeng](guoyuefeng@kaihongdigi.com)
- [深开鸿-孙鑫(深开鸿-孙鑫)]()
- [博泰-陆冕]()


## Notes

#### 议题一、知识体系联合立项进展

**进展**

+ **博泰：车机场景Demo（陆冕）**
    已经指定了完整的项目开发计划，原计划在10月底完成初步开发；考虑到HDC等节奏,建议开发进度提前推进1-2周.展会演示可能只需要一块RK3568开发板和流畅的网络即可。  
**遗留问题**  
    需要内部进行确认提前进度。

#### 议题二、知识体系工作组知识体系梳理（张前福）

**进展**

**VOD声音:**  
+ 新接触的开发者找不到相关资料，仓库众多，资料层次多
+ 参与共建的开发者在代码提交、开发环境搭建、开发板选择与上手等方面存在阻塞
+ 开发者对OpenHarmony的特殊能力不了解，没有对应的梳理
 
**解决方案:**
    知识体系已经有开发样例、学习路径、在线视频等内容，没有进行很好的归类整理。后续按照以下类别整理相关内容： 
+ OpenHarmony第一课：在本单元中，开发者可以了解什么是OpenHarmony、OpenHarmony项目组的社区构成、OpenHarmony的HelloWorld,让开发者了解OpenHarmony的设计理念、学会如何参与到OpenHarmony社区中。  
+ OpenHarmony基础知识：在本单元中，开发者可以体验OpenHarmony的基础能力，包含构建框架、内核、文件系统、驱动、网络、UI等操作系统常用的能力。  
+ OpenHarmony典型特性：在本单元中，开发者可以接触到OpenHarmony的分布式等典型特性，体验OpenHarmony的独特魅力。  
+ OpenHarmony典型场景样例：在本单元中，开发者可以通过系列场景Demo,体验广大开发者使用OpenHarmony为各个场景做的解决方案。  
+ 参与知识体系共建：共建相关流程以及规则。

**结论**
    相关内容骨架上传到knowledge仓库，知识体系的相关专家一起参与审视与建设

**[本次会议录播视频](https://pan.baidu.com/s/13qQxmYZxPKVrAGsnlt8CgQ?pwd=6ept)**

## Action items
